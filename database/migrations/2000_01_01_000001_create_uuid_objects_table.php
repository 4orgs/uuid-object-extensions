<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUuidObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uuid_objects', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->string('universal_name');
            $table->string('universal_shortname');
            $table->string('universal_type');
            $table->string('universal_class');
            $table->string('universal_description');
            $table->uuid('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('uuid_objects');
            $table->timestamps();
        });
        Schema::create('uuid_object_properties', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('object_id');
            $table->foreign('object_id')->references('id')->on('uuid_objects');
            $table->string('property_name');
            $table->string('property_value');
            $table->string('property_sort');
            $table->timestamps();
        });
        Schema::create('uuid_acl_roles', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('secured_id');
            $table->foreign('secured_id')->references('id')->on('uuid_objects');
            $table->string('role_name');
            $table->string('role_description');
            $table->timestamps();
        });
        Schema::create('uuid_acl_user_roles', function (Blueprint $table) {
            $table->uuid('id');
            $table->primary('id');
            $table->uuid('secured_id');
            $table->foreign('secured_id')->references('id')->on('uuid_objects');
            $table->uuid('role_id');
            $table->foreign('role_id')->references('id')->on('uuid_acl_roles');
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('base_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uuid_acl_user_roles');
        Schema::dropIfExists('uuid_acl_roles');
        Schema::dropIfExists('uuid_object_properties');
        Schema::dropIfExists('uuid_objects');
    }
}
