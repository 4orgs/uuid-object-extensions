<?php

namespace Fororgs\UuidObjectExtensions;

use Illuminate\Database\Eloquent\Model;

class UuidObject extends Model
{
    protected $table = 'uuid_objects';
    //

    /*
     * This function is used internally by Eloquent models to test if the model has auto increment value
     * @returns bool Always false
     */
    public function getIncrementing()
    {
        return false;
    }


    /*
     *
     * @returns
     */
    public function properties()
    {
        return $this->hasMany('Fororgs\UuidObjectExtensions\UuidObjectProperty','object_id')->orderBy('property_sort');
        //return $this->hasMany('App\UuidObjectExtensions\UuidObjectProperty','object_id');
    }

    /*
     *
     * @returns
     */
    public function aclUserRoles()
    {
        return $this->hasMany('Fororgs\UuidObjectExtensions\UuidAclUserRole','secured_id');
    }

    /*
     *
     * @returns
     */
    public function children()
    {
        return $this->hasMany(static::class,'parent_id');
    }
 
    /*
     *
     * @returns
     */
    public function parent()
    {
        return $this->belongsTo(static::class,'parent_id');
    }

}
