<?php

namespace Fororgs\UuidObjectExtensions;

class UuidFunctions {

  public static function intToAny( $num, $base = null, $index = null ) {
    if ( bccomp($num,'0') != 1 ) return '0';
    if ( ! $index )
        $index = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if ( ! $base )
        $base = strlen( $index );
    else
        $index = substr( $index, 0, $base );
    $res = '';
    while( bccomp($num,'0') == 1 ) {
        $char = bcmod( $num, $base );
        $res .= substr( $index, $char, 1 );
        $num = bcsub( $num, $char );
        $num = bcdiv( $num, $base );
    }
    return $res;
  }

}