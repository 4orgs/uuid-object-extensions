<?php

namespace Fororgs\UuidObjectExtensions;

use Illuminate\Database\Eloquent\Model;

class UuidAclRole extends Model
{
    //
    protected $table = 'uuid_acl_roles';
    use UuidObjectModelTrait;
}
