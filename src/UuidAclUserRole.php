<?php

namespace Fororgs\UuidObjectExtensions;

use Illuminate\Database\Eloquent\Model;

class UuidAclUserRole extends Model
{
    //
    protected $table = 'uuid_acl_user_roles';
    use UuidObjectModelTrait;
}
