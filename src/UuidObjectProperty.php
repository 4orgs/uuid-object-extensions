<?php

namespace Fororgs\UuidObjectExtensions;

use Illuminate\Database\Eloquent\Model;

class UuidObjectProperty extends Model
{
    //
    protected $table = 'uuid_object_properties';

    use UuidObjectModelTrait;
}
