<?php

namespace Fororgs\UuidObjectExtensions;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

/*
 * This trait is to be used with the default $table->uuid('id') schema definition
 * @package UuidObjectExtensions
 * @author Richard Wendel <richard@wendelservices.com>
 * @license don't use unless I give it to you, don't distribute unless I say you can
 */
trait UuidObjectModelTrait
{

    /*
     * Trait Properties
     */
    public $baseUuidObject;

    /*
     * This function is used internally by Eloquent models to test if the model has auto increment value
     * @returns bool Always false
     */
    public function getIncrementing()
    {
        return false;
    }

    /*
     * This function overwrites the default boot static method of Eloquent models. It will hook
     * the creation event with a simple closure to insert the UUID
     */
    public static function bootUuidObjectModelTrait()
    {
        static::creating(function ($model) {

            // This is necessary because on \Illuminate\Database\Eloquent\Model::performInsert
            // will not check for $this->getIncrementing() but directly for $this->incrementing
            $model->incrementing = false;
            try {
                $model->attributes[$model->getKeyName()] = Uuid::uuid4();
            } catch (UnsatisfiedDependencyException $e) {
                echo 'Caught Exception ' . $e->getMessage() . "\n";
            }
            $model->baseUuidObject = new UuidObject();
            // $modelParentObject->universalName = $model->name;
            $model->baseUuidObject->id = $model->id;
            $model->baseUuidObject->universal_class = $model->table;
            $model->baseUuidObject->save();
            
            
        }, 0);

        static::saving(function ($model) {
            // What's that, trying to change the UUID huh?  Nope, not gonna happen.
            $original_uuid = $model->getOriginal('id');

            if ($original_uuid !== $model->id) {
                $model->id = $original_uuid;
            }
            //$model->baseUuidObject = UuidObject::find($model->id);
            //dd($model->baseUuidObject);
            //$model->baseUuidObject->save();
        }, 0);

        static::deleted(function ($model) {
            $model->baseUuidObject = UuidObject::find($model->id);
            $model->baseUuidObject->delete();
        }, 0);
        
    }

    /*
     * 
     * @returns
     */
    public function uuidObject()
    {
        return $this->hasOne('Fororgs\UuidObjectExtensions\UuidObject','id','id');
        //return UuidObject::find($this->id);
    }

    /*
     * Override to allow ACL management
     * @returns bool Always false unless overriden by child
     */
//    public function getAclManagementAllowed()
//    {
//        return false;
//    }

    
}
